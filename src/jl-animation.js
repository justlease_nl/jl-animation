/**
 * @licence ISC
 * @version 1.1.2
 * @author Erwin Goossen <goossen@justlease.nl>
 */


/*jslint nomen: true */
/*global window, document */
var jlAnimation = (function () {
    'use strict';
    var init = true,
        elements,
        lastPosition,
        requestBusy = false;

    function _getComputedTranslateZ(obj) {
        if(!window.getComputedStyle) return;
        var style = getComputedStyle(obj),
            transform = style.transform || style.webkitTransform || style.mozTransform,
            mat = transform.match(/^matrix\((.+)\)$/);

        return mat ? ~~(mat[1].split(', ')[5]) : 0;
    }

    /**
     *
     * @private
     */
    function _onScroll() {
        lastPosition = window.scrollY;
        _requestScroll();
    }

    function _requestScroll() {
        if (!requestBusy) {
            requestAnimationFrame(_handleScroll);
            requestBusy = true;
        }
    }

    /**
     * We handle the actual stuff over here.
     * We/'re looping through all the elements and add the show class if the
     * element is in our viewport. The init class is set if the element is
     * within the viewport on loading the page.
     *
     * @return {void}
     */
    function _handleScroll() {
        var theHeight = window.innerHeight,
            i = 0,
            obj,
            startAnimation,
            offset;

        for (i = elements.length - 1; i >= 0; i -= 1) {
            obj = elements[i];
            startAnimation = obj.getBoundingClientRect().top;
            offset = +obj.dataset.jlAnimateOffset || 0;

            if ((startAnimation + offset - _getComputedTranslateZ(obj)) < theHeight) {
                if (init) {
                    obj.classList.add('init');
                }
                obj.classList.add('show');
            }
        }

        // Set init to false after our first run
        init = false;
        requestBusy = false;
    }

    /**
     * Set the eventbinding for scrolling
     */
    function _setEventBindings() {
        window.addEventListener('scroll', _onScroll, false);
    }

    /**
     * Do some initialization. Create an array of all the animatable elements,
     * run the init scroll to show the vissible items
     * and set the event bindings for scrolling
     *
     * @return {void}
     */
    function _init() {
        elements = document.querySelectorAll('[data-jl-animate]');
        _handleScroll();
        _setEventBindings();
    }

    _init();
}());
