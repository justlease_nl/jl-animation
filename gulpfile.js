var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

gulp.task('scripts', function () {
    'use strict';
    return gulp.src('src/jl-animation.js')
        .pipe(uglify())
        .pipe(rename('jl-animation.min.js'))
        .pipe(gulp.dest('build'));
});
