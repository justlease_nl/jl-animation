# jlAnimation

## About

A small vanilla JS library for triggering css animations on scrolling elements into a document.

## Usage

Install the library by running `bower install https://bitbucket.org/justlease_nl/jl-animation.git`. For development add the `src/jl-animation.js` to your project. for deployment add the minified version `build/jlanimation.min.js` to you project.
Or download the latest version over here: [Downloads](https://bitbucket.org/justlease_nl/jl-animation/downloads)

Within the HTML you need to set `data-jl-animate` attributes to the elements you wwould like to animate. You can use `data-jl-animate-offset` for some custom offset. And can do your styling.

>### Attributes
>* **data-jl-animate**: you can give this attribuet any name you like. This is used in your stylesheet to create the animation
>* **data-jl-animate-offset**: Numeric value for the offset in pixels. This is from the bottom of the viewport

## Style & Animations
All the animations are done using css styling. There are some defauts to keep in minified

    [data-jl-animate] {
      transition: all .6s ease;
      opacity: 0;
    }

The global animation start. This will sets the element to invisible `opacity:0;` and the default animation settings.

	[data-jl-animate].init {
	  transition-duration: 0s;
	}

Adding the `init` class is set on initializing is the element already is within the viewport. If init is set we do not want to animate and us e speed of 0 seconds.

	[data-jl-animate].show {
	  opacity: 1;
	}

The `show` class is set after the bottom of the viewport has been reached (with taking account of the offset).

Beside the default you can create your own animations with their own names. Here's a little axample:

	[data-jl-animate=slideleft] {
	  transform: translateX(-100%);
	}

This will move the element it's full width to the left. After the `show` class has been edded it needs to set in place again by adding the next piece of css:

	[data-jl-animate=slideleft].show {
	  transform: translateX(0);
	}

Within the example folder there's an example file.